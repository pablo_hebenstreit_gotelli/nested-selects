class NestedSelects extends HTMLElement {
    constructor() {
        super();
        this.obj = {};
        this.innerHTML = `
        <style>
        .accordeon {
            width: 460px;
            height: auto;
            min-height: 340px;
            font-size: 20px;
            cursor: pointer;
            user-select: none;
            -moz-user-select: none;
            -khtml-user-select: none;
            -webkit-user-select: none;
            -o-user-select: none;
            display: block;
            position: relative;
            z-index: 10;
          }
          
          .accordeon-header {
            display: inline-block;
            width: 450px;
            border: solid 0.1vw black;
            border-radius: 0.2vw;
            background-color: white;
            padding-left: 10px;
            color: black;
          }
          
          .accordeon-header:hover {
            opacity: 0.7;
          }
          
          .accordeon-body {
            display: block;
            position: absolute;
          }
          
          .button-group {
            display: block;
            cursor: pointer;
            width: 460px;
            text-align: left;
            font-size: 20px;
            font-weight: bold;
          }
          
          .accordeon-group {
            padding-left: 20px;
          }
          
          .accordeon-group .button-group {
            width: 100%;
          }
          
          .button-group[depth="-1"] {
            color: green;
          }
          
          .hide {
            display: none;
          }
        </style>
        <div class="accordeon">
            <span class="accordeon-header">Proyectos</span>
            <div class="accordeon-body hide"></div>
        </div>
        `;        

        const accordeonAddEventsToggleHeaders = () => {

            Array.from(document.getElementsByClassName("accordeon-header")).forEach(function (header) {
                if (header.getAttribute("listener") !== "true") {
                    header.addEventListener("click", function () {
                        this.parentNode.getElementsByClassName("accordeon-body")[0].classList.toggle("hide");
                    });
                    header.setAttribute("listener", "true");
                }
            });

        }
        const accordeonAddEvents = () => {
            accordeonAddEventsToggleHeaders()
            
            Array.from(document.getElementsByClassName("button-group")).forEach(function (but) {
                if (but.getAttribute("listener") !== "true") {
                    but.addEventListener("click", function () {
                        if (this.getAttribute("depth") === "-1") {
                            let header = this;
                            while ((header = header.parentElement) && header.className !== "accordeon");
                            header.getElementsByClassName("accordeon-header")[0].innerHTML = this.innerHTML;
                            return;
                        }
                        const groups = Array.from(this.parentNode.getElementsByClassName("accordeon-group"));
                        groups.forEach(g => {
                            if (g.getAttribute("uuid") === this.getAttribute("uuid") &&
                                g.getAttribute("depth") === this.getAttribute("depth")) {
                                g.classList.toggle("hide");
                            }
                        });
                    });
                    but.setAttribute("listener", "true");
                }
            });
        }

        const initAccordeon = (data) => {
            let accordeons = Array.from(document.getElementsByClassName("accordeon-body"));
            accordeons.forEach(acc => {
                acc.innerHTML = "";
                const route = (subObj, keyIndex = 0, parent = acc, depth = 0) => {
                    const keys = Object.keys(subObj);
                    if (typeof subObj === 'object' && !Array.isArray(subObj) && keys.length > 0) {
                        while (keyIndex < keys.length) {
                            const but = document.createElement("button");
                            but.className = "button-group";
                            but.setAttribute("uuid", keyIndex);
                            but.setAttribute("depth", depth);
                            but.innerHTML = keys[keyIndex];
                            const group = document.createElement("div");
                            group.className = "accordeon-group hide";
                            group.setAttribute("uuid", keyIndex);
                            group.setAttribute("depth", depth);
                            route(subObj[keys[keyIndex]], 0, group, depth + 1);
                            keyIndex++;
                            parent.append(but);
                            parent.append(group);
                        }
                    } else {
                        if (!Array.isArray(subObj)) subObj = [subObj];
                        subObj.forEach((e, i) => {
                            if (typeof e === 'object') {
                                route(e, 0, parent, depth);
                            } else {
                                const but = document.createElement("button");
                                but.className = "button-group";
                                but.setAttribute("uuid", i);
                                but.setAttribute("depth", "-1");
                                but.innerHTML = e;
                                parent.append(but);
                            }
                        });
                    }
                };
                route(data);
            });
            accordeonAddEvents();
        }

        initAccordeon(obj);
    }
}
customElements.define("nested-selects", NestedSelects);