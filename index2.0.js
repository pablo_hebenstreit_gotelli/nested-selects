class NestedSelects extends HTMLElement {
    constructor() {
        super();
        this.innerHTML = `
        <style>
        .accordeon {
            width: 460px;
            height: auto;
            min-height: 340px;
            font-size: 20px;
            cursor: pointer;
            user-select: none;
            -moz-user-select: none;
            -khtml-user-select: none;
            -webkit-user-select: none;
            -o-user-select: none;
            display: block;
            position: relative;
            z-index: 10;
          }
          
          .accordeon-header {
            display: inline-block;
            width: 450px;
            border: solid 0.1vw black;
            border-radius: 0.2vw;
            background-color: white;
            padding-left: 10px;
            color: black;
          }
          
          .accordeon-header:hover {
            opacity: 0.7;
          }
          
          .accordeon-body {
            display: block;
            position: absolute;
          }
          
          .button-group {
            display: block;
            cursor: pointer;
            width: 460px;
            text-align: left;
            font-size: 20px;
            font-weight: bold;
          }
          
          .accordeon-group {
            padding-left: 20px;
          }
          
          .accordeon-group .button-group {
            width: 100%;
          }
          
          .button-group[depth="-1"] {
            color: green;
          }
          
          .hide {
            display: none;
          }
        </style>
        <div class="accordeon">
            <span class="accordeon-header">Proyectos</span>
            <div class="accordeon-body hide"></div>
        </div>
        <main>
            <ul>
                <li data-client="SANTANDER">SANTANDER
                    <ul>
                        <li data-1="Santander 1">Santander 1</li>
                        <li data-2="Santander 2">Santander 2</li>
                        <li>
                            <ul>
                                <li data-1="Santander 2-1">Santander 2-1</li>
                                <li data-2="Santander 2-2">Santander 2-2</li>
                                <li data-3="Santander 2-3">Santander 2-3
                                    <ul>
                                        <li data-1="Santander 2-3-1">Santander 3-1</li>
                                        <li data-2="Santander 2-3-2">Santander 3-2</li>
                                        <li data-3="Santander 2-3-3">Santander 3-3</li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="ul-headers" data-client="BBVA" data-index=1>BBVA</li>
                <li class="ul-headers" data-client="BANKINTER" data-index=2>BANKINTER</li>
            </ul>

        </main>
        `; 
        const agregarAtributosAUl = () => {
            console.log(document.querySelectorAll('ul'))
            const ul = document.querySelectorAll('ul')
            ul.forEach(function(item,index){
                // Set data-* attribute using the dataSet API
                //item.dataset.head = item.previousElementSibling.textContent;
                item.setAttribute("index", index)
                item.setAttribute("class", "client")
                const text = document.getElementsByClassName('client')[index].innerText;
                console.log(item);
                console.log(text);
              });
            //ul.setAttribute("data", 1);
            console.log(ul[0])
            /* Array.from(document.querySelector('ul')).forEach((item) => {
               
                console.log(item)
                ul.setAttribute("data", index);
                //console.log(document.document.querySelector('ul').getAttribute("data"))
            }); */
        }      

        /* const headerElements = document.getElementsByClassName("ul-headers");
        const my_data_array_headers = []
        for(let header of headerElements){
            my_data_array_headers.push(Object.assign({},header.dataset))
        }
        const firstChild = document.getElementsByClassName("first-child");
        const my_data_array_firstChild = []
        for(let child of firstChild){            
            my_data_array_firstChild.push(Object.assign({},child.dataset))
        }
        const secondChild = document.getElementsByClassName("second-child");
        const my_data_array_secondChild = []
        for(let child of secondChild){
            my_data_array_secondChild.push(Object.assign({},child.dataset))
        }
        const thirdChild = document.getElementsByClassName("third-child");
        const my_data_array_thirdChild = []
        for(let child of thirdChild){
            my_data_array_thirdChild.push(Object.assign({},child.dataset))
        } */
        
        // console.log(my_data_array_headers)
        //console.log(my_data_array_firstChild)        
        // console.log(my_data_array_secondChild)        
        // console.log(my_data_array_thirdChild)
        

        /* const firstChildArray = []
        my_data_array_firstChild.forEach((child, i) => {
            if(my_data_array_firstChild.length-1 >=child.index){
               firstChildArray.push(child)
            }
        }) 
        console.log('firstChildArray ',firstChildArray)
        const secondChildArray = []
        my_data_array_secondChild.forEach((child, i) => {
            if(my_data_array_secondChild.length-1 >=child.index){
               secondChildArray.push(child)
            }
        }) 
        //console.log('secondChildArray ',secondChildArray)
        const thirdChildArray = []
        my_data_array_thirdChild.forEach((child, i) => {
            if(my_data_array_thirdChild.length-1 >=child.index){
               thirdChildArray.push(child)
            }
        })  */
        //console.log('thirdChildArray ',thirdChildArray)
        /* my_data_array_thirdChild.map((item, i) => {console.log(item[i+1], item.index)}) */
        /* const newObject = () => {
            let object = {...my_data_array_headers}
            console.log(object, 'object')
            my_data_array_headers.map((item, i) => {
                //console.log(firstChildArray[i].index.includes(i))
                if(firstChildArray[i].index.includes(i)){
                    console.log('hola')
                    //my_data_array_headers[i] = my_data_array_headers[i].firstChildArray
                    console.log(my_data_array_headers[i][i+1])
                    my_data_array_headers[i].firstChild = firstChild
                    //my_data_array_headers[i].firstChild
                    console.log(my_data_array_headers[i][i+1])
                }       
                console.log('adios')

                //console.log(item[i+1], item.index)
            })
            return object            
        }
        console.log('newobject ', newObject()) */
        

       const obj = {
            "SANTANDER": {
                "Santander 1": "Santander 1",
                "Santander 2": [
                    "Santander 2-1",
                    "Santander 2-2",
                    {
                        "Santander 2-3": 
                        [
                            "Santander 2-3-1",
                            "Santander 2-3-2",
                            "Santander 2-3-3",
                        ],
                    }
                ]
            },
            "BBVA": "BBVA",
            "BANKINTER": "BANKINTER"
        };

        const accordeonAddEventsA = () => {

            Array.from(document.getElementsByClassName("accordeon-header")).forEach(function (header) {
                if (header.getAttribute("listener") !== "true") {
                    header.addEventListener("click", function () {
                        this.parentNode.getElementsByClassName("accordeon-body")[0].classList.toggle("hide");
                    });
                    header.setAttribute("listener", "true");
                }
            });

        }
        const accordeonAddEvents = () => {
            accordeonAddEventsA()
            Array.from(document.getElementsByClassName("button-group")).forEach(function (but) {
                if (but.getAttribute("listener") !== "true") {
                    but.addEventListener("click", function () {
                        if (this.getAttribute("depth") === "-1") {
                            let header = this;
                            while ((header = header.parentElement) && header.className !== "accordeon");
                            header.getElementsByClassName("accordeon-header")[0].innerHTML = this.innerHTML;
                            return;
                        }
                        const groups = Array.from(this.parentNode.getElementsByClassName("accordeon-group"));
                        groups.forEach(g => {
                            if (g.getAttribute("uuid") === this.getAttribute("uuid") &&
                                g.getAttribute("depth") === this.getAttribute("depth")) {
                                g.classList.toggle("hide");
                            }
                        });
                    });
                    but.setAttribute("listener", "true");
                }
            });
        }

        const initAccordeon = (data) => {
            let accordeons = Array.from(document.getElementsByClassName("accordeon-body"));
            accordeons.forEach(acc => {
                acc.innerHTML = "";
                const route = (subObj, keyIndex = 0, parent = acc, depth = 0) => {
                    const keys = Object.keys(subObj);
                    if (typeof subObj === 'object' && !Array.isArray(subObj) && keys.length > 0) {
                        while (keyIndex < keys.length) {
                            const but = document.createElement("button");
                            but.className = "button-group";
                            but.setAttribute("uuid", keyIndex);
                            but.setAttribute("depth", depth);
                            but.innerHTML = keys[keyIndex];
                            const group = document.createElement("div");
                            group.className = "accordeon-group hide";
                            group.setAttribute("uuid", keyIndex);
                            group.setAttribute("depth", depth);
                            route(subObj[keys[keyIndex]], 0, group, depth + 1);
                            keyIndex++;
                            parent.append(but);
                            parent.append(group);
                        }
                    } else {
                        if (!Array.isArray(subObj)) subObj = [subObj];
                        subObj.forEach((e, i) => {
                            if (typeof e === 'object') {
                                route(e, 0, parent, depth);
                            } else {
                                const but = document.createElement("button");
                                but.className = "button-group";
                                but.setAttribute("uuid", i);
                                but.setAttribute("depth", "-1");
                                but.innerHTML = e;
                                parent.append(but);
                            }
                        });
                    }
                };
                route(data);
            });
            accordeonAddEvents();
            agregarAtributosAUl()
        }

        initAccordeon(obj);
    }
}
customElements.define("nested-selects", NestedSelects);