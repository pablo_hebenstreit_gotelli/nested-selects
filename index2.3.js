class NestedSelects extends HTMLElement {
    constructor() {
        super();
        this.innerHTML = `
        <style>
        .accordeon {
            width: 460px;
            height: auto;
            min-height: 340px;
            font-size: 20px;
            cursor: pointer;
            user-select: none;
            -moz-user-select: none;
            -khtml-user-select: none;
            -webkit-user-select: none;
            -o-user-select: none;
            display: block;
            position: relative;
            z-index: 10;
          }
          
          .accordeon-header {
            display: inline-block;
            width: 450px;
            border: solid 0.1vw black;
            border-radius: 0.2vw;
            background-color: white;
            padding-left: 10px;
            color: black;
          }
          
          .accordeon-header:hover {
            opacity: 0.7;
          }
          
          .accordeon-body {
            display: block;
            position: absolute;
          }
          
          .button-group {
            display: block;
            cursor: pointer;
            width: 460px;
            text-align: left;
            font-size: 20px;
            font-weight: bold;
          }
          
          .accordeon-group {
            padding-left: 20px;
          }
          
          .accordeon-group .button-group {
            width: 100%;
          }
          
          .button-group[depth="-1"] {
            color: green;
          }
          
          .hide {
            display: none;
          }
        </style>
        <div class="accordeon">
            <span class="accordeon-header">Proyectos</span>
            <div class="accordeon-body hide"></div>
        </div>
        <main>
            <ul id="list">
                <li>SANTANDER universidades
                    <ul>
                        <li>Santander 1</li>
                        <li>Santander 2</li>
                        <li>
                            <ul>
                                <li>Santander 2-1</li>
                                <li>Santander 2-2</li>
                                <li>Santander 2-3
                                    <ul>
                                        <li>Santander 2-3-1</li>
                                        <li>Santander 2-3-2</li>
                                        <li>Santander 2-3-3</li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li>BBVA</li>
                <li>BANKINTER</li>
            </ul>

        </main>
        `; 

        const getTitlesList = () => {
            const ul = document.querySelectorAll('ul')
            const titleArray = []
            Array.from(ul).forEach(item => {
                titleArray.push(ul[0].childNodes)
            })
            const titleStringArray = [];
            Array.from(titleArray[0]).forEach(item => {
                titleStringArray.push(item.textContent.split('\n')[0].replace('\n','').replace(' ','-')); 
            })
            return titleStringArray.filter(word => word !=='')
        } 

        const setTitleList = () => {
            const titlesList = getTitlesList();
            const list = document.querySelectorAll('#list');
            Array.from(list[0].children).forEach((item, index) => {
                list[0].children[index].setAttribute(`data-${titlesList[index]}`, titlesList[index])  
            })
            //setSublevels();
            iterateDOMTree();
            setTitleObject();
        } 

        let mainObject2 = {}
        const setTitleObject = () => {
            const titlesList = getTitlesList();
            console.log(titlesList)
            titlesList.forEach((item, index) => {
                mainObject2 = {...mainObject2, [titlesList[index]]: titlesList[index]}
            })
            console.log(JSON.stringify(mainObject2))
        }
        let mainObject = {}
        const processItem = (item) => {
            const itemKey = item.innerHTML.split('\n')[0]
            let objeto = {}
            const titlesList = getTitlesList();
            if(item.children.length){
                //console.log(item)
                Array.from(item.children[0].children).forEach((subItem, index) => {
                    //console.log(item.children[0].parentNode.parentElement.textContent)
                    //item.setAttribute(`data-${titlesList[index]}`, '') 
                    let value = subItem.textContent
                    objeto = {...objeto, [value]:''} 
                    
                    //console.log(objeto)
                    //objecTarray.push(subItem.textContent)
                    processItem(subItem);
                })
            } else {
                let value = item.textContent
                    objeto = {...objeto, [value]:value}
                    mainObject = {... mainObject, [itemKey]: itemKey}
                    
                    //console.log(objeto)
                //console.log(item.textContent)
                //objecTarray.push(item.textContent)
            }
            //mainObject = {... mainObject, objeto}
            //console.log(mainObject)
        }

        const iterateDOMTree = () => {       
            const titlesList = getTitlesList();
            const list = document.querySelectorAll('#list');
            console.log(list[0].children)
            Array.from(list[0].children).forEach((item, index) => {
                //console.log(item, 'iterateDOMtree')
                processItem(item)
                //console.log(item.children[0].setAttribute('depth', index+1))
                //console.log(item.length)
                /* if(item.children.length){
                    //console.log(item.children[0], '///////////')
                    Array.from(item.children[0].children).forEach((subItem) => {
                        //console.log(subItem)
                        if(!subItem.childElementCount){
                            //console.log(subItem.textContent)
                            subItem.setAttribute(`data-${titlesList[index]}-${[index+1]}`, subItem.textContent)
                        }
                        //console.log(subItem)
                        Array.from(subItem).forEach((item2) => { //no es un array ARREGLAR
                            //console.log(item2.textContent)
                            if(!item2.childElementCount){
                                //subItem.setAttribute(`data-${titlesList[index]}-${[index+1]}`, subItem.textContent)
                            }
                            //console.log(item2.textContent)
                            console.log(item2.children[0])
                            item2.setAttribute(`data-${titlesList[index]}-${[index+2]}`, subItem.textContent)
                        })
                    })
                    //console.log(item.children[0].tagName==='UL')
                    //console.log('tiene UL')
                } 
                //console.log('no tiene') */
            })
            console.log("iterateDOMTree")
        }

        /* const setSublevels = () => {
            const titlesList = getTitlesList();
            const list = document.querySelectorAll('#list');
            list[0].children[0].getAttribute('title-first-level')
            //console.log(list[0].children[0].getAttribute('title-first-level'))
            //console.log(list[0].children)
            titlesList.forEach(function(item, index){
                //console.log(item.includes(list[0].children[index].getAttribute('title-first-level')))
                //console.log(item)
                Array.from(list[0].children).forEach(item => {
                    //console.log(item)
                })
               
                for(let subitem of list[0].children){
                    if(subitem.textContent.split(' ')[0].replace('\n','') === item){
                        if(subitem.childNodes.length >=2){
                            //console.log(subitem.childNodes[1].tagName === 'UL')
                        }
                    }
                    //console.log(subitem.textContent.split(' ')[0].replace('\n',''))               
                }
            })
            const sublevelNode = []
            //console.log("sublevel")
        } */

        const addAttributesToIdList = () => { 
            const ul = document.querySelectorAll('ul')
            const li = document.querySelectorAll('li')
            ul.forEach(function(item, index){
                //item.setAttribute("index", index)
                //item.setAttribute("class", item.children[0].textContent.split(' ')[0])
                //console.log(item.children[0].textContent.split(' ')[0])
                //console.log(item[0])
                /* item.firstChild.forEach(function(subItem, subIndex){
                    //subItem.setAttribute("sub-index", subIndex)
                    //subItem.setAttribute("class", "projects")
                    //console.log(subItem)
                    
                }) */
                /* li.forEach(function(subItem, subIndex){
                    //subItem.setAttribute("sub-index", subIndex)
                    //subItem.setAttribute("class", "projects")
                    //console.log(subItem)
                    
                }) */
                /* const textA = document.getElementsByClassName('client')[index].innerText;
                const text = textA.slice('')
                const childElements = item.children
                console.log(childElements[index].firstChild.nodeValue);
                console.log(text); */
              });
            //ul.setAttribute("data", 1);
            //console.log(ul[0])
            /* Array.from(document.querySelector('ul')).forEach((item) => {
               
                console.log(item)
                ul.setAttribute("data", index);
                //console.log(document.document.querySelector('ul').getAttribute("data"))
            }); */
        }      

       const obj = {
            "SANTANDER": {
                "Santander 1": "Santander 1",
                "Santander 2": [
                    "Santander 2-1",
                    "Santander 2-2",
                    {
                        "Santander 2-3": 
                        [
                            "Santander 2-3-1",
                            "Santander 2-3-2",
                            "Santander 2-3-3",
                        ],
                    }
                ]
            },
            "BBVA": "BBVA",
            "BANKINTER": "BANKINTER"
        };

        const accordeonAddEventsA = () => {

            Array.from(document.getElementsByClassName("accordeon-header")).forEach(function (header) {
                if (header.getAttribute("listener") !== "true") {
                    header.addEventListener("click", function () {
                        this.parentNode.getElementsByClassName("accordeon-body")[0].classList.toggle("hide");
                    });
                    header.setAttribute("listener", "true");
                }
            });

        }
        const accordeonAddEvents = () => {
            accordeonAddEventsA()
            Array.from(document.getElementsByClassName("button-group")).forEach(function (but) {
                if (but.getAttribute("listener") !== "true") {
                    but.addEventListener("click", function () {
                        if (this.getAttribute("depth") === "-1") {
                            let header = this;
                            while ((header = header.parentElement) && header.className !== "accordeon");
                            header.getElementsByClassName("accordeon-header")[0].innerHTML = this.innerHTML;
                            return;
                        }
                        const groups = Array.from(this.parentNode.getElementsByClassName("accordeon-group"));
                        groups.forEach(g => {
                            if (g.getAttribute("uuid") === this.getAttribute("uuid") &&
                                g.getAttribute("depth") === this.getAttribute("depth")) {
                                g.classList.toggle("hide");
                            }
                        });
                    });
                    but.setAttribute("listener", "true");
                }
            });
        }

        const initAccordeon = (data) => {
            let accordeons = Array.from(document.getElementsByClassName("accordeon-body"));
            accordeons.forEach(acc => {
                acc.innerHTML = "";
                const route = (subObj, keyIndex = 0, parent = acc, depth = 0) => {
                    const keys = Object.keys(subObj);
                    if (typeof subObj === 'object' && !Array.isArray(subObj) && keys.length > 0) {
                        while (keyIndex < keys.length) {
                            const but = document.createElement("button");
                            but.className = "button-group";
                            but.setAttribute("uuid", keyIndex);
                            but.setAttribute("depth", depth);
                            but.innerHTML = keys[keyIndex];
                            const group = document.createElement("div");
                            group.className = "accordeon-group hide";
                            group.setAttribute("uuid", keyIndex);
                            group.setAttribute("depth", depth);
                            route(subObj[keys[keyIndex]], 0, group, depth + 1);
                            keyIndex++;
                            parent.append(but);
                            parent.append(group);
                        }
                    } else {
                        if (!Array.isArray(subObj)) subObj = [subObj];
                        subObj.forEach((e, i) => {
                            if (typeof e === 'object') {
                                route(e, 0, parent, depth);
                            } else {
                                const but = document.createElement("button");
                                but.className = "button-group";
                                but.setAttribute("uuid", i);
                                but.setAttribute("depth", "-1");
                                but.innerHTML = e;
                                parent.append(but);
                            }
                        });
                    }
                };
                route(data);
            });
            accordeonAddEvents();
            addAttributesToIdList();
            setTitleList();
        }

        initAccordeon(obj);
    }
}
customElements.define("nested-selects", NestedSelects);