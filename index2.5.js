class NestedSelects extends HTMLElement {
    constructor() {
        super();
        this.innerHTML = `
        <style>
        .accordeon {
            width: 460px;
            height: auto;
            min-height: 340px;
            font-size: 20px;
            cursor: pointer;
            user-select: none;
            -moz-user-select: none;
            -khtml-user-select: none;
            -webkit-user-select: none;
            -o-user-select: none;
            display: block;
            position: relative;
            z-index: 10;
          }
          
          .accordeon-header {
            display: inline-block;
            width: 450px;
            border: solid 0.1vw black;
            border-radius: 0.2vw;
            background-color: white;
            padding-left: 10px;
            color: black;
          }
          
          .accordeon-header:hover {
            opacity: 0.7;
          }
          
          .accordeon-body {
            display: block;
            position: absolute;
          }
          
          .button-group {
            display: block;
            cursor: pointer;
            width: 460px;
            text-align: left;
            font-size: 20px;
            font-weight: bold;
          }
          
          .accordeon-group {
            padding-left: 20px;
          }
          
          .accordeon-group .button-group {
            width: 100%;
          }
          
          .button-group[depth="-1"] {
            color: green;
          }
          
          .hide {
            display: none;
          }
        </style>
        <div class="accordeon">
            <span class="accordeon-header">Proyectos</span>
            <div class="accordeon-body hide"></div>
        </div>
        <main>
            <slot name="list"></slot>

        </main>
        `; 

        let mainObject = {};

        const processItem = (item) => {
        const itemKey = item.innerHTML.split('\n')[0];
        //console.log('current item is: ', itemKey);

        let itemObject;
        //let itemObjectArray = [];
        //itemObjectArray.push(itemKey);

        if(item.children.length) {
            //itemObject = {[itemKey]:itemKey, children: []};
            let arrayFromChildrens = [];
            Array.from(item.children[0].children).forEach((subItem, index) => {
                //itemObject.children.push(processItem(subItem));
                arrayFromChildrens.push(processItem(subItem));
            });
            itemObject ={[itemKey]:arrayFromChildrens};
        } else  {
            itemObject = {[itemKey]:itemKey};
        }

        return itemObject;

        }
         

        const iterateDOMTree = () => {       
            //const titlesList = getTitlesList();
            const list = document.querySelectorAll('#list');
            //console.log(list[0].children)
            Array.from(list[0].children).forEach((item, index) => {
                 //mainObjectArray.push(processItem(item));
                 const itemKey = item.innerHTML.split('\n')[0];
                 const elementLeveo0 = processItem(item);
                 mainObject[itemKey] = elementLeveo0[itemKey];

            })
            /* console.log("iterateDOMTree completed");
            console.log('JSON main object ARRAY is: ', mainObjectArray);
            console.log('JSON main object is: ', mainObject);
            console.log('JSON main object is: ', JSON.stringify(mainObject)); */
            return mainObject
        }  

        const obj = iterateDOMTree() 

        const accordeonAddEvents = () => {

            Array.from(document.getElementsByClassName("accordeon-header")).forEach(function (header) {
                if (header.getAttribute("listener") !== "true") {
                    header.addEventListener("click", function () {
                        this.parentNode.getElementsByClassName("accordeon-body")[0].classList.toggle("hide");
                    });
                    header.setAttribute("listener", "true");
                }
            });
            
            Array.from(document.getElementsByClassName("button-group")).forEach(function (but) {
                if (but.getAttribute("listener") !== "true") {
                    but.addEventListener("click", function () {
                        if (this.getAttribute("depth") === "-1") {
                            let header = this;
                            while ((header = header.parentElement) && header.className !== "accordeon");
                            header.getElementsByClassName("accordeon-header")[0].innerHTML = this.innerHTML;
                            return;
                        }
                        const groups = Array.from(this.parentNode.getElementsByClassName("accordeon-group"));
                        groups.forEach(g => {
                            if (g.getAttribute("uuid") === this.getAttribute("uuid") &&
                                g.getAttribute("depth") === this.getAttribute("depth")) {
                                g.classList.toggle("hide");
                            }
                        });
                    });
                    but.setAttribute("listener", "true");
                }
            });
        }

        const initAccordeon = (data) => {
            let accordeons = Array.from(document.getElementsByClassName("accordeon-body"));
            accordeons.forEach(acc => {
                acc.innerHTML = "";
                const route = (subObj, keyIndex = 0, parent = acc, depth = 0) => {
                    const keys = Object.keys(subObj);
                    if (typeof subObj === 'object' && !Array.isArray(subObj) && keys.length > 0) {
                        while (keyIndex < keys.length) {
                            const but = document.createElement("button");
                            but.className = "button-group";
                            but.setAttribute("uuid", keyIndex);
                            but.setAttribute("depth", depth);
                            but.innerHTML = keys[keyIndex];
                            const group = document.createElement("div");
                            group.className = "accordeon-group hide";
                            group.setAttribute("uuid", keyIndex);
                            group.setAttribute("depth", depth);
                            route(subObj[keys[keyIndex]], 0, group, depth + 1);
                            keyIndex++;
                            parent.append(but);
                            parent.append(group);
                        }
                    } else {
                        if (!Array.isArray(subObj)) subObj = [subObj];
                        subObj.forEach((e, i) => {
                            if (typeof e === 'object') {
                                route(e, 0, parent, depth);
                            } else {
                                const but = document.createElement("button");
                                but.className = "button-group";
                                but.setAttribute("uuid", i);
                                but.setAttribute("depth", "-1");
                                but.innerHTML = e;
                                parent.append(but);
                            }
                        });
                    }
                };
                route(data);
            });
            accordeonAddEvents();
            iterateDOMTree();
        }

        initAccordeon(obj);
    }
}
customElements.define("nested-selects", NestedSelects);